package com.capacitacion.fs.prueba.services;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.capacitacion.fs.prueba.models.Alumno;
import com.capacitacion.fs.prueba.projections.AlumnoCorto;
import com.capacitacion.fs.prueba.repositories.AlumnoRepository;


@Service
public class AlumnoServices {

	//Inyección de dependencias mediante atributo
	private AlumnoRepository alumnoRepository;

	public AlumnoServices(AlumnoRepository alumnoRepository) {
		this.alumnoRepository = alumnoRepository;
	}
	
	public Alumno guardar(Alumno alumno) {
		if(alumnoRepository.findByCedula(alumno.getCedula())!=null) {
			return null;
		}
		return alumnoRepository.save(alumno);
	}
	
	public List<Alumno> listar(){
		return (List<Alumno>) alumnoRepository.findAll();
	}
	
	public Alumno actualizar(Alumno alumno) {
		return alumnoRepository.save(alumno);
	}
	
	public void eliminar(Long id) {
		alumnoRepository.deleteById(id);
	}
	
	public Alumno obtenerPorId(Long id) {

		return alumnoRepository.findById(id).isPresent() ? alumnoRepository.findById(id).get() : null;
	}
	
	public List<AlumnoCorto> alumnosCortos(){
		return alumnoRepository.alumnosCortos();
	}

	public Page<AlumnoCorto> alumnosCortos(Pageable page){
		return alumnoRepository.findAlumnosProjectedBy(page);
	}

	public AlumnoCorto findCortoById(Long id){
		return alumnoRepository.findCortoById(id);
	}

	public AlumnoCorto findCortoByNombre(String nombre){
		return alumnoRepository.findCortoByNombre(nombre);
	}
}
