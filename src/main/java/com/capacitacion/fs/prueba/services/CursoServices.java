package com.capacitacion.fs.prueba.services;

import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;

import com.capacitacion.fs.prueba.models.Curso;
import com.capacitacion.fs.prueba.repositories.CursoRepository;


@Service
public class CursoServices {

	//Inyección de dependencias mediante atributo
	private CursoRepository cursoRepository;

	public CursoServices(CursoRepository cursoRepository) {
		this.cursoRepository = cursoRepository;
	}
	
	public Curso guardar(Curso curso) {
		if(cursoRepository.findByNombre(curso.getNombre())!=null) {
			return null;
		}
		return cursoRepository.save(curso);
	}
	
	public List<Curso> listar(){
		return (List<Curso>) cursoRepository.findAll();
	}
	
	public Curso actualizar(Curso curso) {
		return cursoRepository.save(curso);
	}
	
	public void eliminar(Long id) {
		cursoRepository.deleteById(id);
	}
	
	public Curso obtenerPorId(Long id) {
		
		return cursoRepository.findById(id).isPresent() ? cursoRepository.findById(id).get() : null;
	}

}
