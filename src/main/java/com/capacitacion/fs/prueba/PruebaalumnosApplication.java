package com.capacitacion.fs.prueba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaalumnosApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaalumnosApplication.class, args);
	}

}

