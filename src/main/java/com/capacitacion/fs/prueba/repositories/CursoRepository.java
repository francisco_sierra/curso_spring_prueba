package com.capacitacion.fs.prueba.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.capacitacion.fs.prueba.models.Curso;

//Le avisa a Spring que esta clase va a manejar el repositorio de Rol
//en CrudRepository<Rol, Long>,
//el primer parámetro es el nombre de la clase que se va a manejar en el repositorio
//el segundo parametro es el tipo de campo de la clave primaria
@Repository
public interface CursoRepository extends CrudRepository<Curso, Long>{
	
	Curso findByNombre(String nombre);
		
}