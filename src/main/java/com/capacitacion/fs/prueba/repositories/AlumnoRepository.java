package com.capacitacion.fs.prueba.repositories;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.capacitacion.fs.prueba.models.Alumno;
import com.capacitacion.fs.prueba.projections.AlumnoCorto;

//Le avisa a Spring que esta clase va a manejar el repositorio de Rol
//en CrudRepository<Rol, Long>,
//el primer parámetro es el nombre de la clase que se va a manejar en el repositorio
//el segundo parametro es el tipo de campo de la clave primaria
@Repository
public interface AlumnoRepository extends CrudRepository<Alumno, Long>{
	
	Alumno findByCedula(String cedula);

	AlumnoCorto findCortoByCedula(String cedula);
	AlumnoCorto findCortoById(Long id);
	AlumnoCorto findCortoByNombre(String nombre);

	@Query("Select a.nombre as nombre,a.apellido as apellido, a.fechanacimiento as fechanacimiento from Alumno a")
	List<AlumnoCorto> alumnosCortos();

	Collection<AlumnoCorto> findAllAlumnoCortoBy();

	Page<AlumnoCorto> findAlumnosProjectedBy(Pageable pageable);
}