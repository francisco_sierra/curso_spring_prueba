package com.capacitacion.fs.prueba.projections;

import java.util.Date;

import org.springframework.data.rest.core.config.Projection;

import com.capacitacion.fs.prueba.models.Alumno;

public interface AlumnoCorto {
	String getNombre();
	String getApellido();
	//Date getFechanacimiento();
}
