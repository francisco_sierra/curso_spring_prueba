package com.capacitacion.fs.prueba.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

@Entity
@Data
@Table(name="ALUMNO")
public class Alumno {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	private String cedula;	
	private String nombre;	
	private String apellido;
	//@JsonFormat(pattern = "dd-MM-yyyy")
	private Date fechanacimiento;
	
	@ManyToOne
	@JoinColumn(name= "curso")
	private Curso curso;
	private String email;
	public Long getId() {
		return id;
	}
}
