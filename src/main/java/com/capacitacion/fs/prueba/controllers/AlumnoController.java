package com.capacitacion.fs.prueba.controllers;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capacitacion.fs.prueba.exception.ModelNotFoundException;
import com.capacitacion.fs.prueba.models.Alumno;
import com.capacitacion.fs.prueba.projections.AlumnoCorto;
import com.capacitacion.fs.prueba.responses.Response;
import com.capacitacion.fs.prueba.services.AlumnoServices;

@RestController
@RequestMapping("/api/alumnos/")
public class AlumnoController {

	private AlumnoServices alumnoServices;

	public AlumnoController(AlumnoServices alumnoServices) {
		this.alumnoServices = alumnoServices;
	}
	
	@PostMapping
	public ResponseEntity<Alumno> guardar(@RequestBody @Validated Alumno alumno){
		Alumno newAlumno=alumnoServices.guardar(alumno);
		if(newAlumno==null) {
			throw new DataIntegrityViolationException("Ya existe un alumno con cedula: "+ alumno.getCedula());
		} 
		return ResponseEntity.status(HttpStatus.CREATED).body(newAlumno);
	}
	
	@GetMapping
	public ResponseEntity<List<Alumno>> listar(){
		List<Alumno> lista =alumnoServices.listar();
		return ResponseEntity.status(HttpStatus.OK).body(lista);
	}
	
	@GetMapping("/alumnoscortos")
	public ResponseEntity<List<AlumnoCorto>> alumnosCortos(){
		List<AlumnoCorto> lista = alumnoServices.alumnosCortos();
		return ResponseEntity.status(HttpStatus.OK).body(lista);
	}

	@GetMapping("/page/{pagenumber}")
	public ResponseEntity<Page<AlumnoCorto>> alumnosCortos(@PathVariable(value = "pagenumber") int pageNumber){
		Pageable page=new PageRequest(pageNumber,5);
		Page<AlumnoCorto> lista = alumnoServices.alumnosCortos(page);
		return ResponseEntity.status(HttpStatus.OK).body(lista);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Alumno> obtenerPorId(@PathVariable(value = "id") Long id){
		Alumno alumno = alumnoServices.obtenerPorId(id);
		if(alumno==null) {
			throw new ModelNotFoundException("No se encontró el alumno con ID: "+id);
		}
		return new ResponseEntity<>(alumno,HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<Alumno> actualizar(@RequestBody @Validated Alumno alumno){
		Alumno updateAlumno = alumnoServices.actualizar(alumno); 
		return new ResponseEntity<Alumno>(updateAlumno,HttpStatus.OK);
	}
	
	@DeleteMapping(path="/{id}")
	public ResponseEntity<Response> eliminar(@PathVariable(value="id") Long id){
		alumnoServices.eliminar(id);
		
		Response response = new Response();
		response.setMensaje("Alumno eliminado correctamente");
		response.setEstado(200);
		response.setError("");
		return ResponseEntity.ok(response);
	}

	@GetMapping("/cortos/{id}")
	public ResponseEntity<AlumnoCorto> findCortoById(@PathVariable(value = "id") Long id){
		AlumnoCorto alumno = alumnoServices.findCortoById(id);
		if(alumno==null) {
			throw new ModelNotFoundException("No se encontró el alumno con ID: "+id);
		}
		return new ResponseEntity<>(alumno,HttpStatus.OK);
	}
}
