package com.capacitacion.fs.prueba.controllers;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.capacitacion.fs.prueba.exception.ModelNotFoundException;
import com.capacitacion.fs.prueba.models.Curso;
import com.capacitacion.fs.prueba.services.CursoServices;

@RestController
@RequestMapping("/api/cursos")
public class CursoController {

	private CursoServices cursoServices;

	public CursoController(CursoServices cursoServices) {
		this.cursoServices = cursoServices;
	}
	
	@PostMapping
	public ResponseEntity<Curso> guardar(@RequestBody @Validated Curso curso){
		Curso newCurso=cursoServices.guardar(curso);
		if(newCurso==null) {
			throw new DataIntegrityViolationException("Ya existe un curso con nombre: "+ curso.getNombre());
		} 
		return ResponseEntity.status(HttpStatus.CREATED).body(newCurso);
	}
	
	@GetMapping
	public ResponseEntity<List<Curso>> listar(){
		List<Curso> lista =cursoServices.listar();
		return ResponseEntity.status(HttpStatus.OK).body(lista);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Curso> obtenerPorId(@PathVariable(value = "id") Long id){
		Curso curso = cursoServices.obtenerPorId(id);
		if(curso==null) {
			throw new ModelNotFoundException("No se encontró el curso con ID: "+id);
		}
		return new ResponseEntity<Curso>(curso,HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<Curso> actualizar(@RequestBody @Validated Curso curso){
		Curso updateCurso = cursoServices.actualizar(curso); 
		return new ResponseEntity<Curso>(updateCurso,HttpStatus.OK);
	}
	
	@DeleteMapping(path="/{id}")
	public void eliminar(@PathVariable(value="id") Long id){
		cursoServices.eliminar(id);
	}
}
