INSERT INTO public.curso (id, nombre) VALUES (1, 'A1');
INSERT INTO public.curso (id, nombre) VALUES (2, 'A2');

INSERT INTO public.alumno (id, apellido, cedula, email, nombre, curso, fechanacimiento) VALUES (1, 'Francisco', '1715591523', 'email@mail.org', 'TurnOff', 2, '2009-11-12 19:00:00.000000');
INSERT INTO public.alumno (id, apellido, cedula, email,  nombre, curso, fechanacimiento) VALUES (2, 'Daniel', '6456487710', 'fran@mail.ru', 'comexplicarle', 2, '2019-05-29 19:00:00.000000');
INSERT INTO public.alumno (id, apellido, cedula, email,  nombre, curso, fechanacimiento) VALUES (3, 'Jose', '8437511193', 'francisco@elele.org', 'Jose', 1, '2019-04-30 19:00:00.000000');
INSERT INTO public.alumno (id, apellido, cedula, email,  nombre, curso, fechanacimiento) VALUES (4, 'Miguel', '2034926773', 'yamla@me.com', 'Jose', 2, '2019-04-30 19:00:00.000000');
INSERT INTO public.alumno (id, apellido, cedula, email,  nombre, curso, fechanacimiento) VALUES (5, 'Andres', '4370559852', 'yruan@yahoo.com', 'Miguel', 2, '2019-04-30 19:00:00.000000');
INSERT INTO public.alumno (id, apellido, cedula, email,  nombre, curso, fechanacimiento) VALUES (6, 'Pedro', '7794349436', 'pajrtes@verizon.net', 'Andres', 1, '2019-04-30 19:00:00.000000');
INSERT INTO public.alumno (id, apellido, cedula, email,  nombre, curso, fechanacimiento) VALUES (7, 'Gabriel', '7862415545', 'stakasa@verizon.net', 'Pedro', 1, '2019-04-30 19:00:00.000000');
INSERT INTO public.alumno (id, apellido, cedula, email,  nombre, curso, fechanacimiento) VALUES (8, 'Gabriela', '3951800495', 'jramio@outlook.com', 'Gabriel', 2, '2019-04-30 19:00:00.000000');
INSERT INTO public.alumno (id, apellido, cedula, email,  nombre, curso, fechanacimiento) VALUES (9, 'Daniela', '6972280493', 'gordonjcp@sbcglobal.net', 'Gabriela', 2, '2019-04-30 19:00:00.000000');
INSERT INTO public.alumno (id, apellido, cedula, email,  nombre, curso, fechanacimiento) VALUES (10, 'Pierina', '8066000650', 'chance@icloud.com', 'Daniela', 2, '2019-04-30 19:00:00.000000');
INSERT INTO public.alumno (id, apellido, cedula, email,  nombre, curso, fechanacimiento) VALUES (11, 'Daniela', '4268453740', 'calin@msn.com', 'Pierina', 1, '2019-04-30 19:00:00.000000');
INSERT INTO public.alumno (id, apellido, cedula, email,  nombre, curso, fechanacimiento) VALUES (12, 'Andrea', '3113486523', 'burniske@mac.com', 'Daniela', 2, '2019-04-30 19:00:00.000000');

